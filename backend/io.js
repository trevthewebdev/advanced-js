import _ from 'lodash';

/*
  games = {
    ianisnafasfn: {
      votes: 0
    }
  };
*/

let users = {};
let games = {};

// io === everyone, socket === individual person / browser window
export const onConnect = io => function ioConnect(socket) {
  // user requests a login
  socket.on('server:request-login', onRequestLogin(io, socket));
  // user wants to commit their selections & move onto real-time voting
  socket.on('server:confirm-selections', confirmSelections(io, socket));
  // user is voting for a game
  socket.on('server:user-cast-vote', applyGameVote(io));
  // user is finished voting, check if everyone else has finished as well
  socket.on('server:user-voting-complete', checkifVotingComplete(io, socket));
  // user is closing the browser & disconnecting
  socket.on('disconnect', onDisconnect(io, socket));
}

/* business logic */
function onRequestLogin(io, socket) {
  return ({ username }) => {
    const user = {
      id: socket.id,
      username,
      status: 'unconfirmed'
    };
    // add user to users object representing connected users
    users[socket.id] = user;
    // attach user to the socket they're using so we can identify them later
    socket.user = user;
    // notify the user they successfully logged in
    socket.emit('server:successful-login', user);
    // notify everyone else of the new user
    io.emit('server:users-updated', users);
  }
}

function onDisconnect(io, socket) {
  return function() {
    const { user } = socket;

    if (user) {
      // remove the user from the user object
      delete users[user.id];
      // notify the client app of the user leaving
      io.emit('server:users-updated', users);
    }

    if (_.isEmpty(users)) {
      games = {};
    }
  }
}

/*
  selections = ['aqiqu-32950-238532-328573', '852735-32950-238532-328573']
*/
function confirmSelections(io, socket) {
  return function(selections) {
    // removing duplicate games but saving their votes
    const updates = (
      selections.reduce((prev, id) => {
        if (games[id]) {
          prev[id] = { votes: games[id]['votes'] + 1 };
          return prev;
        }

        prev[id] = { votes: 0 };
        return prev;
      }, {})
    );
    // set new update to games object
    games = { ...games, ...updates };
    // set user's voting status to ready
    users[socket.id].status = 'ready';
    // check and see if voting is ready to begin (all users have confirmed their selections)
    let unconfirmed = Object.keys(users).find(socketId => users[socketId].status === 'unconfirmed');
    // if everyone has a "ready" status start the voting
    if (!unconfirmed) {
      io.emit('server:voting-start', games);
    }
  }
}

function applyGameVote(io) {
  return function({ gameId, votes }) {
    games[gameId].votes += votes;
    io.emit('server:voting-start', games);
  }
}

function checkifVotingComplete(io, socket) {
  return function() {
    users[socket.user.id].status = 'confirmed';
    const unconfirmed = Object.keys(users).find(userId => users[userId].status !== 'confirmed');

    if (!unconfirmed) {
      const winners = getVotingResults(games);
      io.emit('server:voting-results', winners);
    }
  }
}

function getVotingResults(games) {
  let highestVote = 0;
  let winners = [];

  Object.keys(games).forEach(gameId => {
    if (games[gameId].votes > highestVote) {
      highestVote = games[gameId].votes;
      winners = [gameId];
    }

    if (games[gameId].votes === highestVote) {
      winners.push(gameId);
    }
  });

  return winners;
}
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
import { onConnect } from './io';
const PORT = 4208;

app.use(express.static('public'));

io.on('connection', onConnect(io));

server.listen(PORT, () => {
  console.log(`app listening on port http://localhost:${PORT}`);
});

const path = require('path');
const _ = require('lodash');

module.exports = function(env) {
  const classfolder = _.last(process.argv);

  return {
    mode: 'development',
    devtool: 'source-map',

    entry: {
      main: path.resolve(__dirname, `public/${classfolder}/src/js/app.js`)
    },

    watch: true,
    watchOptions: {
      ignored: /node_modules/
    },

    output: {
      filename: '[name].js',
      chunkFilename: '[name].bundle.js',
      path: path.resolve(__dirname, `public/${classfolder}/dist`),
      publicPath: `dist/`
    },

    module: {
      rules: [
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        }
      ]
    }
  };
}

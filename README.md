# Introduction to Javascript

Make sure you have node installed... http://nodejs.org

## Windows Installation
1. Open cmd
2. `cd Desktop`
3. `git clone https://gitlab.com/trevthewebdev/advanced-js`
4. `cd advanced-js`
5. `npm install` - wait until it's done installing
6. `npm start`
7. Open another a separate cmd
8. `cd Desktop/advanced-js`
9. `npm run watch class1`
    - Change class number to e.g. class2, class3 to compile code for different classes
10. open browser and go to http:localhost:3208

## Mac OSX Installation
1. Open terminal
2. `cd ~/Desktop`
3. `git clone https://gitlab.com/trevthewebdev/advanced-js`
4. `cd advanced-js`
5. `npm install` - wait until it's done installing
6. `npm start`
7. Open another a separate terminal
8. `cd ~/Desktop/advanced-js`
9. `npm run watch class1`
    - (Change class number to e.g. class2, class3 to compile code for different classes)
10. open browser and go to http:localhost:3208

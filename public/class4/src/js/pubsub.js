/**
 * Pubsub Module
 *
 * Description:
 * Publish / Subscribe (pubsub) is a mechanism that allows some code to publish
 * and event while others subscribe (listen) for that event. When the event occurs
 * those listening can then react to it.
 *
 * Example Usage:
 * Subscribe: var subscription = pubsub.subscribe('/page/load', function(data) { // React to event here // });
 * Publish: pubsub.publish('/page/load', { prop: value, ... });
 *
 */

const topics = {};
const hop = topics.hasOwnProperty;

console.log('topics:', topics);

export function subscribe(topic, listener) {
  /* create the topic object if not yet created */
  if (!hop.call(topics, topic)) {
    topics[topic] = [];
  }

  /* add the event listener to the queue */
  const index = topics[topic].push(listener) - 1;

  return {
    remove: function remove() {
      delete topics[topic][index];
    }
  };
}

export function publish(topic, info) {
  /* if the topic doesn't exist, or there's no listeners in the queue, juse leave */
  if (!hop.call(topics, topic)) {
    return;
  }

  /* cycle through each listener (function) in the topic's queue */
  topics[topic].forEach(function cycleTopics(item) {
    item(info !== undefined ? info : {});
  });
}

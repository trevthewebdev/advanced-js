import $ from 'jquery';

/*
  Debounce:
  Description: Only call the function after a certain amount of time has passed since the last call of the debounce function
*/
function debounce(func, wait, immediate) {
  let timeout;

  return function() {
    const context = this;
    const args = arguments;
    const later = function() {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  }
}

/* higher order function (debounce is also), where we declare a function that can return another function */
function returnFunc(param) {
  console.log('FUNC LEVEL 1', param);

  return function(event) {
    console.log('FUNC LEVEL 2', param);
  }
}

// function currying... calling any inner functions in the same line as the outer call.
const something = returnFunc('hey')();

// function handleEvent(param1, param2, param3) {
//   return function(event) {
//     console.log(param1); // 1
//     console.log(param2); // 2
//     console.log(param3); // 3
//   }
// }
// $(document).on('scroll', handleEvent(1, 2, 3));xw

/* Real example of debounce, keep scrolling then stop for 250ms and look in the console */
function handleScroll(event) {
  console.log('got called!', event);
}
$(document).on('scroll', debounce(handleScroll, 250));

const x = [];

function grow() {
  console.log('made it');
  for (let i = 0; i < 10000; i++) {
    document.body.appendChild(document.createElement('div'));
  }
  x.push(new Array(1000000).join('x'));
}

// document.getElementById('grow').addEventListener('click', grow);

// import './lesson';
import $ from 'jquery';
import io from 'socket.io-client';
import { dispatch } from '../../../utils';
import { getState, setState } from './state';
import { RequestGameListing } from './services';
import Listing from './gameListing';
import Filters from './gameFilters';
import Selections from './gameSelections';
import Login from './login';
// import './lesson';

const socket = io();

socket.on('server:users-updated', function(users) {
  console.log(users);
});

Login(socket);
Listing.init();
Filters.init();
Selections.init();

const $drawerToggle = $('#drawer-toggle');

$drawerToggle.on('click', handleDrawerToggle);
$drawerToggle.on('keyup', handleDrawerToggle);

function handleDrawerToggle(event) {
  switch (event.which) {
    case 1: // mouse
    case 13: // enter
    case 32: // space
      dispatch('app/drawer/toggle');
      break;
  }
}

const cache = JSON.parse(localStorage.getItem('app-games'));
const hasExpired = new Date().getTime() <= cache && cache.expires;

if (cache && !hasExpired) {
  /* use the games from the cache */
  setState('GameListing', {
    games: cache.games,
    total: cache.total
  });
  /* tell the whole app we updated the games */
  dispatch('/app/listing/update');
} else {
  /* go get the games */
  RequestGameListing()
    .then(function (response) {
      /* update the global state */
      setState('GameListing', {
        games: response.data,
        total: response.meta.total
      });

      /* tell the whole app we updated the games */
      dispatch('/app/listing/update');

      /* cache the games that came back */
      localStorage.setItem('app-games', JSON.stringify({
        games: response.data,
        total: response.meta.total,
        expires: new Date(new Date().getTime() + 1 * 60000).getTime() // expires after (1) minute
      }));
    });
}

/* just updates the count next to the star when a selection is chosen */
window.addEventListener('/app/selections/update', function () {
  const { selections } = getState('GameSelections');
  $drawerToggle.html(`<i class="fa fa-star-o" aria-hidden="true"></i> <span class="show-for-sr">Games Selected:</span> ${selections.length}`);
});

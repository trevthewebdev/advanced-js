'use strict';

import $ from 'jquery';
import { dispatch } from '../../../utils';
import { getState, setState } from './state';
import { RequestGameListing } from './services';

export default {
  init: function init() {
    this.$selects = $('select');
    this.bindEvents();
  },

  bindEvents: function bindEvents() {
    this.$selects.on('change', this.onChange);
  },

  onChange: function onChange(event) {
    const $this = $(this);
    const newFilter = {
      [this.name]: this.value
    };

    /* update the state with the new filters */
    setState('GameListing', {
      filters: Object.assign({}, getState('GameListing').filters, newFilter)
    });

    /* use the updated filters (in the state) to fetch the games */
    RequestGameListing(getState('GameListing').filters)
      .then(response => {
        setState('GameListing', {
          games: response.data,
          total: response.meta.total
        });

        /* tell the whole app we updated the games */
        dispatch('/app/listing/update');
      })
      .catch(error => {
        console.log('error occured when getting games');
      });
  }
}

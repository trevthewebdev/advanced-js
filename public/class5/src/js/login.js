import $ from 'jquery';
import { setState, getState } from './state';

const $body = $('body');
const $loginModal = $('#login-modal');

export default function makeLogin(socket) {
  const Login = {
    init: function init() {
      this.$input = $('[name="username"]');
      this.$form = $('form');
      this.bindEvents();
    },

    bindEvents: function bindEvents() {
      /* listen for submit event for "login" */
      this.$form.on('submit', this.onSubmit.bind(this));

      /* wait for server to respond before letting the user get access to the games */
      socket.on('server:successful-login', this.onSuccessfulLogin);
    },

    onSubmit: function onSubmit(event) {
      event.preventDefault();
      const username = this.$input.val();
      if (this.isValidUsername(username)) {
        socket.emit('server:request-login', { username });
      }
    },

    isValidUsername: function isValidUsername(username) {
      return typeof username === 'string' && username.length >= 4;
    },

    onSuccessfulLogin: function onSuccessfulLogin({ username }) {
      setState('User', { username });
      $body.removeClass('is-overlaying');
      $loginModal.removeClass('is-showing');
    }
  };

  return Login.init();
}

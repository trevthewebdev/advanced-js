import $ from 'jquery';
import { getState } from './state';
import { subscribe } from './pubsub';

const $body = $('body');

export default function makeVoting(socket) {
  const Voting = {
    init: function init() {
      this.$modal = $('#voting-modal');
      this.bindEvents();
    },

    bindEvents: function bindEvents() {
      // listen for cofirmation in the FRONTEND
      subscribe('/app/voting/confirm', this.handleConfirmation.bind(this));
      // listen for voting to start from the BACKEND
      socket.on('server:voting-start', this.handleReadyState.bind(this));
    },

    render: function render(gamesMarkup) {
      this
        .$modal
        .empty()
        .addClass('modal--voting')
        .append(
          $('<div class="modal-content">')
            .append([
              $('<p>')
                .text('Votes are weighted by priority, your first vote is worth 3 while you last is worth 1'),
              $('<ul>')
                .addClass('grid-x grid-margin-x no-bullet list--voting')
                .append(gamesMarkup)
            ])
        );
    },

    handleConfirmation: function handleConfirmation() {
      const { selections } = getState('GameSelections');
      const { width, height } = this.$modal[0].getBoundingClientRect();
      // send user's selections to the BACKEND
      socket.emit('server:confirm-selections', selections);
      // update ui for the user to see the voting modal
      $body.addClass('is-overlaying');

      this
        .$modal
        .addClass('is-showing')
        .css({
          width: `${width}px`,
          height: `${height}px`
        });
    },

    handleReadyState: function handleReadyState(serverGames) {
      const gamesMarkup = _generateGamesMarkup(serverGames);
      this.render(gamesMarkup);
    }
  };

  return Voting.init();
}

function _generateGamesMarkup(serverGames) {
  const $frag = $(document.createDocumentFragment());
  const { games } = getState('GameListing');

  Object.keys(serverGames).forEach(sgId => {
    const game = games.find((game) => sgId === game._id );

    if (game) {
      $frag.append(
        $('<li>')
          .addClass('cell medium-6')
          .append(
            $(`<div data-votes="${serverGames[sgId].votes}" data-vote-id="${sgId}">`)
              .addClass('card--vote')
              .text(game.name)
          )
      );
    }
  });

  return $frag;
}

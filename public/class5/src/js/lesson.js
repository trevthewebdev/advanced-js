import $ from 'jquery';
import _ from 'lodash';

/*
  Debounce:
  Description: Only call the function after a certain amount of time
               has passed since the last call of the debounce function
*/
function debounce(func, wait, immediate) {
  let timeout;

  return function() {
    const context = this;
    const args = arguments;
    const later = function() {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  }
}

function debounceHandler(event) {
  console.log('debounce scroll event!');
}

$(document).on('scroll', debounce(debounceHandler, 250));

/**
 * Throttle
 */
function throttle(func, limit) {
  let isThrottling;

  return function() {
    const args = arguments;
    const context = this;

    if (!isThrottling) {
      func.apply(context, args);
      isThrottling = true;

      setTimeout(() => {
        isThrottling = false;
      }, limit);
    }
  }
}

function throttleHandler(event) {
  console.log('throttle scroll event!');
}

$(document).on('scroll', throttle(throttleHandler, 500));

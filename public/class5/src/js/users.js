import $ from 'jquery';
import { publish } from './pubsub';
import { setState, getState } from './state';

export default function makeUsers(socket) {
  const Users = {
    init: function init() {
      this.$userDrawer = $('#user-drawer');
      this.bindEvents();
    },

    bindEvents: function bindEvents() {
      socket.on('server:users-updated', this.userListUpdates.bind(this));

      this.$userDrawer.on('click', 'button', () => {
        this.$userDrawer.toggleClass('is-open');
      });
    },

    render: function render(users) {
      const $button = (
        $('<button class="button expanded">')
          .text(users.length > 1 ? `${users.length} voters` : `You're all alone`)
      );
      const $content = (
        $('<div class="user-drawer-content">')
          .append(
            $('<ul class="no-bullet">')
              .append(_generateUserList(users))
          )
      );

      this
        .$userDrawer
        .empty()
        .append([ $content, $button ]);
    },

    userListUpdates: function userListUpdates(users) {
      setState('Users', { active: users });
      publish('/app/users/update');
      this.render(users);
    }
  };

  return Users.init();
}

function _generateUserList(users) {
  const $frag = $(document.createDocumentFragment());

  Object.keys(users).forEach(socketId => {
    const { username } = users[socketId];
    const firstLetter = username[0];
    const lastLetter = username[username.length - 1];

    $frag.append(
      $('<li>')
        .attr('data-letters', `${firstLetter}${lastLetter}`)
        .text(username)
    );
  });

  return $frag;
}

'use strict';

import $ from 'jquery';
import { dispatch } from '../../../utils';
import { getState, setState } from './state';

export default {
  init: function init() {
    this.$container = $('#selection-drawer');
    this.$containerList = $('#drawer-game-list');
    this.bindEvents();
  },

  render: function render(html) {
    this.$containerList.empty().append(html);
  },

  bindEvents: function bindEvents() {
    this.$containerList.on('click', '.remove-item-button', this.handleListClick);

    /* events to toggle the "selection drawer" visibility */
    window.addEventListener('app/drawer/toggle', this.toggleDrawer.bind(this));
    $(window).on('keyup', event => {
      switch (event.which) {
        case 27:
          this.toggleDrawer();
          break;
      }
    });

    /* when the user adds a game to their selections */
    window.addEventListener('/app/selections/update', this.updateSelections.bind(this));
  },

  toggleDrawer: function toggleDrawer(action) {
    this.$container.toggleClass('is-open');
  },

  handleListClick: function handleListClick(event) {
    const $this = $(event.target);
    const gameId = $this.data('game-id');

    /* get the selections from the state & make a copy of it to manipulate */
    const state = getState('GameSelections');
    const selections = [...state.selections];

    /* find the index of what we want to remove */
    const index = selections.findIndex(id => id === gameId);

    /* remove it from the copy */
    selections.splice(index, 1);

    setState('GameSelections', { selections });

    // tell the rest of the app there was an update
    dispatch('/app/selections/update', {
      removed: gameId
    });
  },

  updateSelections: function updateSelections(event) {
    const { GameListing, GameSelections } = getState();

    /* loop over the selected game ids in the state */
    const games = GameSelections.selections.map(gameId => {
      /* loop over the listing in the state to find a match for the game ids (this is a nested loop) */
      const match = GameListing.games.find(game => {
        if (game._id === gameId) {
          return {
            _id: game._id,
            name: game.name
          };
        }
      });

      return match;
    });
    const html = _generateListHtml(games);

    /* visually update the listing */
    this.render(html);
  }
};

function _generateListHtml(gamesArray) {
  var $fragment = document.createDocumentFragment();

  gamesArray.forEach(function (game, index) {
    var $li = document.createElement('li');

    $li.classList.add('cell', 'medium-4', 'large-3', 'drawer-item');
    $li.innerHTML = (`
        <div class="media-object">
          <div class="media-object-section">
            <div class="thumbnail">
              <img src="http://placehold.it/50">
          </div>
          </div>
          <div class="media-object-section">
            <h6 class="subheader">${game.name}</h6>
          </div>
        </div>
        <a data-game-id="${game._id}" class="remove-item-button">x</a>
      `)

    $fragment.appendChild($li);
  });

  return $fragment;
}

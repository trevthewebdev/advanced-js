'use strict';

import Axios from 'axios';

export function RequestGameListing(filters) {
  const queryParams = filtersToQueryParams(filters);

  return new Promise((resolve, reject) => {
    Axios
      .get(`https://overtureapp.com/api/v1/games${queryParams}`)
      .then(response => resolve(response.data))
      .catch(error => reject(error));
  });
}

function filtersToQueryParams(filters) {
  if (!filters) {
    return '';
  }

  var keys = Object.keys(filters);
  var queryParamsString = '?';

  return keys.reduce(function (prev, key, i) {
    const atEnd = i + 1 !== keys.length;
    if (filters[key]) {
      return `${prev}filter[${key}]=${filters[key]}${atEnd ? '&' : ''}`;
    }

    return prev;
  }, queryParamsString);
}

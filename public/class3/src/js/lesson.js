/* simple factory pattern */
function vehicleFactory(parent, props) {
  const vehicle = Object.create(parent);

  Object.keys(props).forEach(key => {
    vehicle[key] = props[key];
  });

  return vehicle;
}

/* Parent Object */
const Vehicle = {
  brake: function brake() {
    console.log(`${this.type} is breking...`);
  },
  drive: function drive() {
    console.log(`${this.type} is driving...`);
  }
};

/* Sub Object Level 1 */
const DodgeRam = vehicleFactory(Vehicle, {
  type: 'truck',
  make: 'Dodge',
  model: 'Ram',
  reverse: function reverse() {
    console.log(`${this.type}, ${this.make}, ${this.model} is reversing...`);
  }
});

/* Another Sub Object Level 1 */
const scionxb = vehicleFactory(Vehicle, {
  type: 'car',
  make: 'Scion',
  model: 'xB'
});

/* Sub Object Level 2 */
const dodge2018 = vehicleFactory(DodgeRam, {
  year: 2018
});

console.log(DodgeRam);
console.log(dodge2018);
console.log(scionxb);

DodgeRam.brake();
DodgeRam.drive();
DodgeRam.reverse();

dodge2018.reverse();

scionxb.brake();
scionxb.drive();

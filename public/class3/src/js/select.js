import $ from 'jquery';

const Select = {
  defaults: {
    label: '',
    containerClass: 'Select',
    openClass: 'is-open'
  },

  init: function init($elem, userOptions) {
    this.$container = $elem;
    // verify the options sent in
    _areValidUserOptions(userOptions);
    // set this select instance's "state"
    this.props = { ...this.defaults, ...userOptions };

    $elem.addClass('Select');
    this.render();
    this.bindEvents();

    // returns elem wrapped in jquery
    return $elem;
  },

  bindEvents: function() {
    this.$container.on('click', 'button', this._handleToggle.bind(this));
    this.$container.on('click', 'li', this._handleSelect.bind(this));
  },

  _handleToggle: function _handleToggle() {
    this.$container.toggleClass(this.props.openClass);
  },

  _handleSelect: function _handleSelect(event) {
    const $elem = $(event.target);
    const value = $elem.data('value');

    this.props.selected = value;
    this.render();
    // wrapped in a setTimeout to manipulate the callstack and allow the animation to finish
    setTimeout(() => this._handleToggle(), 0);

    if (typeof this.props.onSelect === 'function') {
      this.props.onSelect(this.props.name, value);
    }
  },

  render: function render() {
    const { props } = this;
    const $options = _generateOptionMarkup(props.options, props.selected);
    const $list = $('<ul class="no-bullet option-list">').append($options);
    const $button = (
      $('<label>')
        .text(props.label)
        .append(
          $('<button>')
            .text(props.selected || '-- any --')
        )
    );

    this.$container
      .empty()
      .append([ $button, $list ]);
  }
};

$.fn.Select = function makeSelect(userOptions) {
  const instance = Object.create(Select);
  return instance.init(this, userOptions);
}

/* private functions */
function _areValidUserOptions(userOptions) {
  const { options } = userOptions;

  /* make sure options is an array */
  if (!Array.isArray(options)) {
    throw '"Select" options is not an array';
  }

  if (options.length) {
    /* loop over and check if they're valid */
    options.forEach(function validateOption(option, index) {
      if (!_isValidOption(option)) {
        throw `option ${option} at index ${index} is not a valid "Select" option`;
      }
    });
  }

  return true;
}

function _isValidOption(option) {
  if (
    typeof option === 'object' &&
    typeof option.label === 'string' &&
    typeof option.value === 'string' &&
    !option.value.match(/ /g)
  ) {
    return true;
  }
}

function _generateOptionMarkup(options, selected) {
  const $frag = $(document.createDocumentFragment());

  options.forEach(({ label, value }) => {
    $frag.append(
      $(`<li ${value === selected && 'class="is-disabled"'}>`)
        .data('value', value)
        .text(label)
    );
  });

  return $frag;
}

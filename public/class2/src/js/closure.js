import { publish } from "./pubsub";

/* closure */
function doSomething() {
  const a = 2;

  function logSomething() {
    console.log(a);
  }

  return logSomething;
}

const closureHappensBro = doSomething();

closureHappensBro();

/* revisting state... a real example of closure */
export function initState() {
  const _state = {
    prop1: {
      subprop: 'value1'
    },

    prop2: {
      subprop: 'value2'
    }
  };

  function getState() {
    return _state;
  }

  function setState(prop, update) {
    _state[prop] = update;
  }

  return {
    getState,
    setState
  };
}

const state = initState();

state.setState('prop1', 'so much update, very much wow');

console.log(state.getState());

(function() {
  console.log('fired...');
  publish('heyd00d', {
    prop: 'value'
  });
})();

setTimeout(() => {
  publish('heyd00d', {
    prop: 'value'
  });
}, 0);

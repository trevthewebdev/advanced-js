/* valinna js (browser) */
window.addEventListener('wow:custom', event => console.log('Vanilla custom event', event.detail));

/* trigger custom vanilla event */
const newEvent = new CustomEvent('wow:custom', { detail: 'get yo deets here' });
window.dispatchEvent(newEvent);

function getComponent() {
  /* we did not go over the dynamic import part in class */
  return import(/* webpackChunkName: "jquery" */ 'jquery').then($ => {
    $ = $.default;

    /* went over this in class */
    $(document).on('much:custom', (event, param1, param2) => console.log('jQuery custom event', param1, param2));
    $(document).trigger('much:custom', ['holy', 'cow']);
    /* end: went over this in class */
  });
}

/* we did not go over this in class */
setTimeout(() => {
  getComponent()
    .then(() => console.log('module loaded'));
}, 2000);

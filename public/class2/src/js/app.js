import { subscribe } from './pubsub';
import './closure';
import './prototypes';
import './custom-events';

const obj1 = {
  prop1: 'val1',
  prop2: 'val2'
};

console.log({
  ...obj1,
  prop3: 'value3'
});

console.log('subscribed!');

subscribe('heyd00d', data => {
  console.log('GOT SOME DATA HERE: ', data);
});

/* utility to extend (copy) classical inheritance */
function extend(subClass, superClass) {
  const F = function() {};
  F.prototype = superClass.prototype;
  subClass.prototype = new F();
  subClass.prototype.constructor = subClass;
}

/* classical inheritance... kind of... */

/* Person class */
function Person(name) {
  this.name = name;
}

Person.prototype = {
  getName: function getName() {
    return this.name;
  }
};

/* Author class */
function Author(name, books) {
  Person.call(this, name);
  this.books = books;
}

extend(Author, Person);

Author.prototype.getBooks = function getBooks() {
  return this.books;
};

const author = new Author('Trevor', ['book1', 'book2']);
window.author = author;
console.log(author);

/* new es6 classes */
class Animal {
  constructor(name) {
    this.name = name;
  }

  speak() {
    console.log(`I am ${this.name}`);
  }
}

class Cat extends Animal {
  constructor(name, type) {
    super(name);
    this.type = type;
  }

  indentify() {
    console.log(`I am a ${this.type} named ${this.name}`);
  }
}

const animal = new Animal('Burt');
animal.speak();

const winston = new Cat('Winston', 'tabby');
winston.speak();
winston.indentify();

/* oloo (objects linked to other object) a.k.a the real prototypes */
const Truck = {
  type: 'truck',
  drive: function drive() {
    console.log(`${this.engine}`);
  },

  brake: function brake() {
    console.log(`${this.make} ${this.model} - ${this.engine} is BRAKING`);
  }
};

const Dodge = Object.create(Truck, {
  engine: {
    value: 'v8'
  },
  make: {
    value: 'Dodge'
  }
});

const Ram = Object.create(Dodge, {
  model: {
    value: 'Ram'
  }
});

Ram.reverse = function reverse() {
  console.log(`${this.make} ${this.model} - ${this.engine} is REVERSING`);
}

console.log(Dodge);
console.log(Ram);

Dodge.drive();
Dodge.brake();

Ram.drive();
Ram.brake();
Ram.reverse();

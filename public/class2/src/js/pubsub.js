const topics = {};
const hop = topics.hasOwnProperty;

console.log('topics: ', topics);

export function subscribe(topic, listener) {
  /* create the topic object if not yet create */
  if (!hop.call(topics, topic)) {
    topics[topic] = [];
  }

  /* add the event listener to the quque */
  const index = topics[topic].push(listener) - 1;

  return {
    remove: function remove() {
      delete topics[topic][index];
    }
  };
}

export function publish(topic, data) {
  /* if the topic doesn't exist, or there's no listeners in the queue, just leave */
  if (!hop.call(topics, topic)) {
    return;
  }

  /* cycle through each listener (function) in the topics queue */
  topics[topic].forEach(function cycleTopics(listener) {
    listener(data !== undefined ? data : {});
  });
}

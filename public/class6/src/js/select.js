'use strict';

import $ from 'jquery';

const Select = {
  defaults: {
    label: '',
    containerClass: 'Select',
    openClass: 'is-open'
  },

  init: function init($elem, userOptions) {
    this.$container = $elem;

    /* verify the options sent in */
    _areValiduserOptions(userOptions)

    /* set this select instance's state */
    this.props = { ...this.defaults, ...userOptions };

    this.$container.addClass('Select');
    this.render();
    this.bindEvents();

    return $elem;
  },

  bindEvents: function bindEvents() {
    this.$container.on('click', 'button', this._handleToggle.bind(this));
    this.$container.on('click', 'li', this._handleSelect.bind(this));
  },

  _handleToggle: function _handleToggle() {
    this.$container.toggleClass(this.props.openClass);
  },

  _handleSelect: function _handleSelect(event) {
    if (!this.$container.hasClass(this.props.openClass)) {
      return;
    }

    const $elem = $(event.target);
    const value = $elem.data('value');

    this.props.selected = value;
    this.render();
    setTimeout(() => this._handleToggle(), 0);

    if (typeof this.props.onSelect === 'function') {
      this.props.onSelect(this.props.name, value);
    }
  },

  render: function render() {
    const { props } = this;
    const $options = _generateOptionMarkup(props.options, props.selected);
    const $list = $('<ul class="no-bullet option-list">').append($options);
    const $button = (
      $('<label>')
        .text(props.label)
        .append(
          $('<button>')
            .text(props.selected || '-- any --')
        )
    );

    this.$container
      .empty()
      .append([ $button, $list ]);
  }
};

/* factory attached to jquery */
$.fn.Select = function makeSelect(userOptions) {
  const instance = Object.create(Select);
  return instance.init(this, userOptions);
}

/* checks if option label && value are valid */
function _areValiduserOptions(userOptions) {
  var isValid = true;

  /* make sure options is an array */
  if (!Array.isArray(userOptions.options)) {
    isValid = false;
  }

  if (userOptions.options.length) {
    /* loop over the options and check if their valid */
    userOptions.options.forEach(function validateOption(option, index) {
      if (!_isValidOption(option)) {
        throw `option: ${option} at index ${index} is not a valid "Select" option`;
        isValid = false;
      }
    });
  }

  return isValid;
}

/* check is an option is an object, has a lable & value that are strings, and the value cannot have spaces */
function _isValidOption(option) {
  if (
    typeof option === 'object' &&
    typeof option.label === 'string' &&
    typeof option.value === 'string' &&
    !option.value.match(/ /g)
  ) {
    return true;
  }
}

function _generateOptionMarkup(options, selected) {
  const $frag = $(document.createDocumentFragment());

  options.forEach(({ label, value }) => {
    $frag.append(
      $(`<li ${value === selected && 'class="is-disabled"'}>`)
        .data('value', value)
        .text(label)
    );
  });

  return $frag;
}

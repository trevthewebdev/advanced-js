'use strict';

import $ from 'jquery';
import './select';
import { dispatch } from '../../../utils';
import { getState, setState } from './state';
import { RequestGameListing } from './services';
import {
  minPlayer,
  maxPlayer,
  minDuration,
  maxDuration
} from './data/selectData.json';

export default {
  init: function init() {
    this.$selects = $('select');

    $('#min-players').Select({
      ...minPlayer,
      onSelect: this.onChange
    });

    $('#max-players').Select({
      ...maxPlayer,
      onSelect: this.onChange
    });

    $('#min-duration').Select({
      ...minDuration,
      onSelect: this.onChange
    });

    $('#max-duration').Select({
      ...maxDuration,
      onSelect: this.onChange
    });
  },

  onChange: function onChange(name, value) {
    const newFilter = { [name]: value };

    /* update the state with the new filters */
    setState('GameListing', {
      filters: Object.assign({}, getState('GameListing').filters, newFilter)
    });

    /* use the updated filters (in the state) to fetch the games */
    RequestGameListing(getState('GameListing').filters)
      .then(response => {
        setState('GameListing', {
          games: response.data,
          total: response.meta.total
        });

        /* tell the whole app we updated the games */
        dispatch('/app/listing/update');
      })
      .catch(error => {
        console.log('error occured when getting games');
      });
  }
}

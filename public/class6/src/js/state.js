'use strict';

import _ from 'lodash';

let _state = {
  GameListing: {
    games: [], // the games that came back from the api
    total: 0, // total count of games that exist in the system
    filters: {} // current filters being applied
  },

  GameSelections: {
    maximum: 2, // default maximum
    selections: [] // games the user has selected
  },

  User: {
    remainingVotes: 2,
    gamesVotedFor: []
  },

  Voting: {
    games: {}
  }
};

/* pulls the entire state, or just the a specifc part of it */
export const getState = key => !key ? _state : _state[key];

/* updates the state */
export const setState = (segmentName, update) => {
  const _update = (
    Object
      .keys(update)
      .reduce((obj, key) => {
        if (_.isArray(update[key])) {
          _state[segmentName][key] = [];
        }

        obj[segmentName][key] = update[key];

        return obj;
      }, { [segmentName]: {} })
  );

  _state = _.merge({}, _state, _update);
}

'use strict';

import $ from 'jquery';
import { getState, setState } from './state';
import { dispatch } from '../../../utils';

export default {
  init: function init() {
    this.$gameList = $('#game-list');
    this.bindEvents();
  },

  render: function render(html) {
    this.$gameList.empty().append(html);
  },

  bindEvents: function bindEvents() {
    window.addEventListener('/app/listing/update', () => {
      const { games } = getState('GameListing');
      const { selections } = getState('GameSelections');
      const html = _generateListHtml(games, selections);

      /* visually update the listing */
      this.render(html);
    });

    /* just here to visually update the list when a game is deselected */
    window.addEventListener('/app/selections/update', ({ detail }) => {
      if (detail && detail.removed) {
        $(`[data-game-id="${detail.removed}"]`)
          .parents('.card--game')
          .removeClass('is-selected');
      }
    });

    /* using event delegation to listen to clicks on the UL but only respond on the button / star */
    this.$gameList.on('click', 'button[data-game-id]', function(event) {
      /* "this" will equal the DOM version of the button, putting $() around it will "wrap it in the jquery object" */
      const gameId = $(this).data('game-id');
      const state = getState('GameSelections');

      /* make a copy of the selections array in the state */
      const selections = [...state.selections];

      /* see if the id already exists in the array */
      const duplicate = selections.find(id => id === gameId);

      if (!duplicate && state.selections.length < state.maximum) {
        $(event.target).parents('.card--game').addClass('is-selected');

        selections.push(gameId);

        /* update the state */
        setState('GameSelections', { selections });

        /* tell the whole app about the update */
        dispatch('/app/selections/update');
      }
    });
  }
};

/* _generateListHtml: will take an array of games and generate the listing in html */
function _generateListHtml(gamesArray, selections = []) {
  const $fragment = $(document.createDocumentFragment());

  gamesArray.forEach(function (game, index) {
    const isSelected = selections.find(id => game._id === id);

    const $li = (
      $('<li></li>')
        .addClass('cell medium-4 large-3')
        .html(`
          <div class="card--game ${isSelected && 'is-selected'}">
            <figure>
              <img src="https://placehold.it/400x250" alt="" />
            </figure>
            <div class="card__content">
              <h6 class="card__title">${game.name}</h6>
              <p>${game.description || 'Spooky scarey skeletons are doing things in their sleep...'}</p>
              <ul class="no-bullet">
                <li>${_formatTimeText(game.min_duration, game.max_duration)}</li>
                <li>${_formatPlayerText(game.player_min, game.player_max)}</li>
              </ul>
            </div>
            <div class="card__circle-actions">
              <button data-game-id="${game._id}">
                <i class="fa"></i>
              </button>
            </div>
          </div>
        `)
    );

    $fragment.append($li);
  });

  return $fragment;
}

function _formatTimeText(minDuration, maxDuration) {
  return (minDuration === maxDuration)
    ? `~${minDuration} min`
    : `${minDuration} - ${maxDuration} min`;
}

function _formatPlayerText(playerMin, playerMax) {
  if (!playerMax) {
    return `${playerMin} - unlimited players`;
  }

  if (playerMax === 1) {
    return 'Single Player';
  }

  return (playerMin === playerMax)
    ? `${playerMin} player`
    : `${playerMin} - ${playerMax} players`;
}

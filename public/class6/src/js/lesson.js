import _ from 'lodash';

function doSomething() {
  return new Promise(function(resolve) {
    setTimeout(() => {
      resolve('All done, here is the value: 42');
    }, _.random(1, 4) * 1000);
  });
}

async function doAsyncThing() {
  try {
    const result = await doSomething();
    console.log(result);
  } catch(error) {
    console.log(error);
  }
}

doAsyncThing();
